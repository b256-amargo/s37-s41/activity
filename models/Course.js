
// DEPENDENCIES
	const mongoose = require("mongoose");

// SCHEMA
	const courseSchema = new mongoose.Schema({
		name: {
			type: String,
			required: [true, "Course Name is required"]
		},

		description: {
			type: String,
			required: [true, "Course Description is required"]
		},

		price: {
			type: Number,
			required: [true, "Price is required"]
		},

		isActive: {
			type: Boolean,
			default: true
		},

		createdOn: {
			type: Date,
			default: new Date()
		},

		enrollees: [
			{
				userId: {
					type: String,
					required: [true, "User ID is required"]
				},
				enrolledOn: {
					type: Date,
					default: new Date()
				}
			}
		]
	});
	// "required" requires the data for this field/property to be included when creating a record
	// "true" defines if the field is required or not. The 2nd element is the message to be displayed if data is not present
	// "new Date()" instantiates a new date that stores the current date and time whenever a course is created in the database


// MODEL / EXPORT
	module.exports = mongoose.model("Course", courseSchema);
	// 1st element is the name of collection to be created on the database (capitalized singular)
	// 2nd element is the name of the schema
	// "module.exports" allows this file to be referenced as module on other files