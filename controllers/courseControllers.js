// DEPENDENCIES
	const Course = require("../models/Course.js");
	const bcrypt = require("bcrypt"); // for hashing the password
	const auth = require("../auth.js"); // for authentication


// ----------------------------------------------------------------------------------------------------

// ACTIVITY (s39)

// CREATING A NEW COURSE

	// module.exports.addCourse = (request) => {
	// 	let newCourse = new Course({
	// 		name: request.body.name,
	// 		description: request.body.description,
	// 		price: request.body.price
	// 	});

	// 	const userData = auth.decode(request.headers.authorization);
	// 	// console.log(userData);
	// 	// console.log(request.body);

	// 	if(userData.isAdmin) {
	// 		return newCourse.save().then((course, err) => {
	// 			if(err){
	// 				return false;
	// 			}
	// 			else{
	// 				return true;
	// 			};
	// 		});
	// 	}
	// 	else{
	// 		return false;
	// 	};
	// };

	// above did not work since courseRoutes expects to receive a promise (from a .then function)
	// above will work if admin=true, but if admin=!true then the else statement will return a boolean (false), which will not be recognized by the .then from courseRoutes
	// solution will be to move the if/else for checking admin on the courseRoutes

// -------------------------------------------

// CREATING A NEW COURSE (FOR ADMINS ONLY)
/*
	Business Logic:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new Course to the database
*/

module.exports.addCourse = (course) => {
	let newCourse = new Course({
		name: course.name,
		description: course.description,
		price: course.price
	});

	return newCourse.save().then((course, err) => {
		if(err){
			return false;
		}
		else{
			return true;
		};
	});
};

// -------------------------------------------

// RETRIEVING/VIEWING ALL COURSES
/*
	Business Logic:
	1. Retrieve all the courses from the database
*/

module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	});
};

// -------------------------------------------

// RETRIEVING/VIEWING ALL ACTIVE COURSES
/*
	Business Logic:
	1. Retrieve all the courses from the database with the property of "isActive" to true
*/

module.exports.getActiveCourses = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	});
};

// -------------------------------------------

// RETRIEVING/VIEWING A SPECIFIC COURSE
/*
	Business Logic:
	1. Retrieve the course that matches the course ID provided from the URL
*/

module.exports.getCourse = (requestParams) => {
// (received from routes) requestParams = request.params(url)
	return Course.findById(requestParams.courseId).then(result => {
	// findById("url" . "/:courseId")
		return result;
	});
};

// -------------------------------------------

// UPDATING A COURSE (FOR ADMINS ONLY)
/*
	Business Logic:
	1. Create a variable "updatedCourse" which will contain the information retrieved from the request body
	2. Find and update the course using the course ID retrieved from the request params property and the variable "updatedCourse" containing the information from the request body
*/

module.exports.updateCourse = (course, paramsId) => {
	
	let updatedCourse = {
		name: course.name,
		description: course.description,
		price: course.price
	}
	// documents should be the exact same (except the one being updated in this case) as the existing docs in the database
	// using "put" replaces the field entirely by what's contained in the new field, thus ^

	return Course.findByIdAndUpdate(paramsId.courseId, updatedCourse).then((result, err) => {
	// 1st argument is the id of field to be updated, 2nd argument is the update to be applied
	// body input now only need the doc to be updated due to the findByIdAndUpdate method
		if(err){
			return false;
		}
		else{
			return true;
		};
	});
};

// -------------------------------------------

// ACTIVITY (s40)

// ARCHIVING A COURSE (FOR ADMINS ONLY)
// In managing databases, it's common practice to soft delete our records and what we would implement in the "delete" operation of our application
// The "soft delete" happens here by simply updating the course "isActive" status into "false" which will no longer be displayed in the frontend application whenever all active courses are retrieved
// This allows us access to these records for future use and hides them away from users in our frontend application
// The use of "hard delete" refers to removing records from our database permanently

module.exports.archiveCourse = (course, paramsId) => {
	
	let archiveCourse = {
		isActive: course.isActive
	};
	// Better activity solution:
	// can instead directly input (isActive: false) to reduce argument to only one (paramsId)

	return Course.findByIdAndUpdate(paramsId.courseId, archiveCourse).then((result, err) => {
		if(err){
			return false;
		}
		else{
			return true;
		};
	});
};