
// DEPENDENCIES
	const User = require("../models/User.js");
	const Course = require("../models/Course.js");
	const bcrypt = require("bcrypt"); // for hashing the password
	const auth = require("../auth.js"); // for authentication


// ----------------------------------------------------------------------------------------------------

// CHECKING IF USER'S EMAIL ALREADY EXISTS IN THE DATABASE
/*
	Business Logic: 
	1. Use mongoose "find" method to find duplicate emails
	2. Use the "then" method to send a response back to the frontend appliction based on the result of the "find" method
*/

	module.exports.checkIfEmailExists = (requestBody) => {
		return User.find({email: requestBody.email}).then(result => {
			if(result.length > 0) {
				return true;
			}
			else{
				return false;
			};
		});
	};

// -------------------------------------------

// USER REGISTRATION
/*
	Business Logic:
	1. Create a new User object using the mongoose model and the information from the request body
	2. Make sure that the password is encrypted
	3. Save the new User to the database
*/

	module.exports.registerUser = (requestBody) => {
		let newUser = new User({
			firstName: requestBody.firstName,
			lastName: requestBody.lastName,
			email: requestBody.email,
			mobileNo: requestBody.mobileNo,
			password: bcrypt.hashSync(requestBody.password, 10)
			// protects the user's password by showing them as random string characters upon viewing
			// 10 is the (standard) value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt the password (in layman terms, its the number of layers of encryption)
		});

		return newUser.save().then((user, err) => {
			if(err){
				return false;
			}
			else{
				return true;
			};
		});
	};

// -------------------------------------------

// USER AUTHENTICATION
/*
	Steps:
	1. Check the database if the user email exists
	2. Compare the password provided in the login form with the password stored in the database
	3. Generate/return a JSON web token if the user is successfully logged in and return false if not
*/

	module.exports.authenticateUser = (requestBody) => {
		return User.findOne({email: requestBody.email}).then(result => {
			if(result == null) {
				return false;
			}
			else {
				const isPasswordCorrect = bcrypt.compareSync(requestBody.password, result.password)
				if(isPasswordCorrect) {
					return {access: auth.createAccessToken(result)};
				}
				else{
					return false;
				};
			};
		});
	};

// -------------------------------------------

// DISPLAYING PROFILE BY ID

	module.exports.getProfile = (data) => {
		return User.findById(data.userId).then(result => {
		// findById(object.property)
		// "data" is from the getProfile(data) on the ROUTES file, userId is its property made there
			if(result == null) {
				return false;
			}
			else {
				result.password = "";
				return result;
			};
		});
	};

// -------------------------------------------

// ENROLLING A USER TO A COURSE
/*
	Business Logic:
	1. Find the document in the database using the user's ID
	2. Add the course ID to the user's enrollment array
	3. Update the document in the MongoDB Atlas Database
*/
// Async await will be used in enrolling the user because we will need to update 2 separate documents when enrolling a user

	module.exports.enrollUser = async (data) => {
		let isUserUpdated = await User.findById(data.userId).then(user => {
			user.enrollments.push({courseId: data.courseId});
			// new field/doc pushed on the database
			return user.save().then((enroll, err) => {
				if(err){
					return false;
				}
				else{
					return true;
				};
			});
		});

		let isCourseUpdated = await Course.findById(data.courseId).then(course => {
			course.enrollees.push({userId: data.userId});
			// new field/doc pushed on the database
			return course.save().then((enroll, err) => {
				if(err){
					return false;
				}
				else{
					return true;
				};
			});
		});

		if(isUserUpdated && isCourseUpdated) {
			return true;
		}
		else{
			return false;
		};
		// will be run AFTER the two async functions above is finished
	};

// -------------------------------------------

// MINI ACTIVITY
// DISPLAYING PROFILE BY EMAIL

	// module.exports.getProfileByEmail = (data) => {
	// 	return User.findOne(data.body.userEmail).then(result => {
	// 		if(result == null) {
	// 			return false;
	// 		}
	// 		else {
	// 			result.password = "";
	// 			return result;
	// 		};
	// 	});
	// };

// ----------------------------------------------------------------------------------------------------

// ACTIVITY (s38)

	// module.exports.getProfile = (requestBody) => {
	// 	return User.findById(requestBody.id).then(result => {
	// 		if(result == null) {
	// 			return false;
	// 		}
	// 		else {
	// 			result.password = "";
	// 			return result;
	// 		};
	// 	});
	// };
