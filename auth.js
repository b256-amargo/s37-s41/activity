
// DEPENDENCIES
	const jwt = require("jsonwebtoken");
	const secret = "CourseBookingAPI";

// EXPORT (JSON Web Token)
/*
	- JSON Web Token or JWT is a way of securely passing information from the server to the frontend or to other parts of server
	- Information is kept secure through the use of the secret code
	- Only the system that knows the secret code that can decode the encrypted information

	- Imagine JWT as a gift wrapping service that secures the gift with a lock
	- Only the person who knows the secret code can open the lock
	- And if the wrapper has been tampered with, JWT also recognizes this and disregards the gift
	- This ensures that the data is secure from the sender to the receiver
*/


	// TOKEN CREATION
	/*
	- Analogy
		Pack the gift and provide a lock with the secret code as the key
	*/
	module.exports.createAccessToken = (user) => {

		const data = {
			id: user._id,
			email: user.email,
			isAdmin: user.isAdmin
		};
		// the payload (where the user data is stored when the token is created)
		// The data will be received from the registration form
		// When the user logs in, a token will be created with user's information


		return jwt.sign(data, secret, {});
		// Generate a JSON web token using the jwt's sign method
		// Generates the token using the form data and the secret code with no additional options provided
		// 1st variable is the payload
		// 2nd is the "secret" code which can only be seen on this file (hardcoded)
		// 3rd are optional values? (example: valid only for x days, errors)
	};

	// --------------------------

	// TOKEN VERIFICATION
	/*
	- Analogy
		Receive the gift and open the lock to verify if the the sender is legitimate and the gift was not tampered with
	*/
	// The token is retrieved from the request header
	// This can be provided in postman under
	// Authorization > Bearer Token
	module.exports.verify = (request, response, next) => {
		let token = request.headers.authorization;
		if(typeof token !== "undefined") {
			console.log(token);
			token = token.slice(7, token.length);
			// "slice" method -> extract a particular section of a string without modifying the original string.
			// "token" value is "bearer (accesstokenvalue)"
			// since the computer can only accept the actual token value, the "token" was sliced starting from index 7 to get only the accesstokenvalue

			return jwt.verify(token, secret, (err, data) => {
			// err first before data as per jwt structure
				if(err) {
					return response.send({auth: "failed"});
				}
				else{
					next();
					// proceed to next action (the req,res after the "/xxx" routes)
				}
			});
		}
		else{
			return response.send({auth: "failed"});
		};
	};

	// --------------------------

	// TOKEN DECRYPTION
	/*
		Analogy:
			Open the gift and get the content
	*/
	module.exports.decode = (token) => {
		if(typeof token !== "undefined") {
			token = token.slice(7, token.length);
			return jwt.verify(token, secret, (err, data) => {
				if(err) {
					return null;
				}
				else{
					return jwt.decode(token, {complete: true}).payload;
					// decode method -> used to obtain info from jwt
					// complete: true -> option that allows to return additional info from the JWT token
					// payload -> contains info provided in the "createAccessToken" (id, email, isAdmin)
				};
			});
		}
		else{
			return null;
		};
	};