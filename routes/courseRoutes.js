
// DEPENDENCIES
	const express = require("express");
	const router = express.Router();
	const courseController = require("../controllers/courseControllers.js");
	const auth = require("../auth.js");

// ----------------------------------------------------------------------------------------------------

// ACTIVITY (s39)

// ROUTE FOR CREATING A COURSE
	// router.post("/create", auth.verify, (request, response) => {
	// 	courseController.addCourse(request).then(resultFromController => response.send(resultFromController));
	// });

// ----------------------------------------------------------------------------------------------------

// ROUTE FOR CREATING A COURSE (FOR ADMINS ONLY)

	router.post("/create", auth.verify, (request, response) => {

		// Sample Method #1
		// Storing the needed data on a variable, recommended method
		const data = {
			course: request.body, // name, desc, price, from input
			isAdmin: auth.decode(request.headers.authorization).isAdmin // decoded, then accessed isAdmin from payload
		};

		if(data.isAdmin){
			courseController.addCourse(data.course).then(resultFromController => response.send(resultFromController));
		}
		else{
			response.send(false);
		};


		// Sample Method #2
		// const userData = auth.decode(request.headers.authorization);
		// courseController.addCourse(request.body, userData.isAdmin).then(resultFromController => response.send(resultFromController));


		// Sample Method #3
		// const userData = auth.decode(request.headers.authorization);
		// courseController.addCourse(request.body, {userAdmin: userData.isAdmin}).then(resultFromController => response.send(resultFromController));
	});

// -----------------------------------------------

// ROUTE FOR RETRIEVING/VIEWING ALL COURSES

	router.get("/all", (request, response) => {
		courseController.getAllCourses().then(resultFromController => response.send(resultFromController));
	});

// -----------------------------------------------

// ROUTE FOR RETRIEVING/VIEWING ALL ACTIVE COURSES

	router.get("/active", (request, response) => {
		courseController.getActiveCourses().then(resultFromController => response.send(resultFromController));
	});

// -----------------------------------------------

// ROUTE FOR RETRIEVING/VIEWING A SPECIFIC COURSE

	router.get("/:courseId", (request, response) => {
	// courseId is only a placeholder name
		courseController.getCourse(request.params).then(resultFromController => response.send(resultFromController));
		// params = url
	});

// -----------------------------------------------

// ROUTE FOR UPDATING A COURSE (FOR ADMINS ONLY)

	router.put("/update/:courseId", auth.verify, (request, response) => {
		const data = {
			course: request.body,
			isAdmin: auth.decode(request.headers.authorization).isAdmin,
			params: request.params
		};
		// storing all needed data in a single variable for consistency

		if(data.isAdmin){
			courseController.updateCourse(data.course, data.params).then(resultFromController => response.send(resultFromController));
			// two arguments needed (data to be updated via body, course id via url)
		}
		else{
			response.send(false);
		};
	});

// -----------------------------------------------

// ACTIVITY (s40)

// ROUTE FOR ARCHIVING A COURSE (FOR ADMINS ONLY)

// A "PATCH" request is used instead of "DELETE" request because of our approach in archiving and hiding the courses from our users by "soft deleting" records instead of "hard deleting" records which removes them permanently from our databases

	router.patch("/archive/:courseId", auth.verify, (request, response) => {

		const data = {
			course: request.body,
			isAdmin: auth.decode(request.headers.authorization).isAdmin,
			params: request.params
		};

		if(data.isAdmin){
			courseController.archiveCourse(data.course, data.params).then(resultFromController => response.send(resultFromController));
			// Better activity solution:
			// input body can be removed (course on data variable, and the 1st argument above) by directly inputting in the controller itself, since we only expect the same input anyway, thus will only be needing one argument here (data.params)
		}
		else{
			response.send(false);
		};
	});


// -----------------------------------------------

// EXPORT
	module.exports = router;