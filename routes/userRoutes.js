
// DEPENDENCIES
	const express = require("express");
	const router = express.Router();
	const userController = require("../controllers/userControllers.js");
	const auth = require("../auth.js");

// ----------------------------------------------------------------------------------------------------

// ROUTE FOR CHECKING IF USER'S EMAIL ALREADY EXISTS IN THE DATABASE
	router.post("/checkEmail", (request, response) => {
		userController.checkIfEmailExists(request.body).then(resultFromController => response.send(resultFromController));
	});

// ROUTE FOR USER REGISTRATION
	router.post("/register", (request, response) => {
		userController.registerUser(request.body).then(resultFromController => response.send(resultFromController));
	});

// ROUTE FOR USER AUTHENTICATION
	router.post("/login", (request, response) => {
		userController.authenticateUser(request.body).then(resultFromController => response.send(resultFromController));
	})

// ROUTE FOR RETRIEVING USER DETAILS WITH AUTHENTICATION / DECRYPTION
	router.get("/details", auth.verify, (request, response) => {
		const userData = auth.decode(request.headers.authorization);
		// after decoding, will get access to the payload (now renamed as userData) from the auth.js file 
		userController.getProfile({userId: userData.id}).then(resultFromController => response.send(resultFromController));
	})

// ROUTE FOR ENROLLING A USER TO A COURSE
	router.post("/enroll", auth.verify, (request, response) => {
		const data = {
			userId: auth.decode(request.headers.authorization).id,
			courseId: request.body.courseId
		}
		// storing needed data (user and course ID) in a single variable
		userController.enrollUser(data).then(resultFromController => response.send(resultFromController));
		// since there is no unneeded doc contained in the variable, data as a whole is used as the argument
	});

// MINI ACTIVITY
// ROUTE FOR RETRIEVING USER DETAILS WITH AUTHENTICATION / DECRYPTION
	// router.get("/miniactivity", auth.verify, (request, response) => {
	// 	const userData = auth.decode(request.headers.authorization);
	// 	userController.getProfileByEmail({userEmail: userData.email}).then(resultFromController => response.send(resultFromController));
	// })

// ----------------------------------------------------------------------------------------------------

// ACTIVITY (s38)

// ROUTE FOR RETRIEVING USER DETAILS
	// router.post("/details", (request, response) => {
	// 	userController.getProfile(request.body).then(resultFromController => response.send(resultFromController));
	// })

// ----------------------------------------------------------------------------------------------------

// EXPORT
	module.exports = router;