
// DEPENDENCIES
	const express = require("express"); // for creating server, also grants access to different methods
	const mongoose = require("mongoose"); // for connecting to cloud database
	const cors = require("cors"); // for the backend app to connect to the frontend app, also allows us to control the app's Cross-Origin Resource Sharing setting
	const userRoutes = require("./routes/userRoutes.js"); // for connecting the file containing the routes
	const courseRoutes = require("./routes/courseRoutes.js"); // for connecting the file containing the routes

// SERVER
	const app = express(); // result of express() func is stored in here

// SERVER PORT
	const port = 4000;

// MIDDLEWARES
	app.use(express.json()); // auto convert to json
	app.use(express.urlencoded({extended:true})); // to allow the app on accepting other data types other than strings and arrays
	app.use("/users", userRoutes); // setting the parent route
	app.use("/courses", courseRoutes); // setting the parent route

// CLOUD DATABASE CONNECTION
	mongoose.connect("mongodb+srv://admin:admin1234@b256amargo.fojeanf.mongodb.net/B256_CourseAPI?retryWrites=true&w=majority", {
	// admin1234 and B256_CourseAPI manually inserted (password and database name (will be created if nonexistent))
		useNewUrlParser: true,
		useunifiedTopology: true
		// allows us to avoid any current and future errors while connecting to MongoDB
	});

// CHECKING THE DATABASE CONNECTION
	let db = mongoose.connection;
	db.on("error", console.error.bind(console, "Connection error"));
	db.once("open", () => console.log(`Connection to the cloud database successful`));

// SERVER LISTENING
	app.listen(port, () => console.log(`API now online on port ${port}`));

